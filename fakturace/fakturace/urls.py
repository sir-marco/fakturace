#from django.conf.urls import patterns, include, url
from django.conf.urls.defaults import *


# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'fakturace.views.fakturaSeznam', name='home'),
    # url(r'^fakturace/', include('fakturace.foo.urls')),
    url(r'^faktura/(?P<faktura_id>\d+)/$', 'fakturace.views.fakturaTisk'),
    #url(r'^cenik/$', 'fakturace.views.cenikTisk'),
    url(r'^cenik/(?P<detail_id>\d*)$', 'fakturace.views.cenikTisk'),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^login/$', 'django.contrib.auth.views.login', {'template_name' : 'login.html'}),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'template_name' : 'logout.html', 'next_page' : '/'}),
    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
